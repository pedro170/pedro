import Navbar from "../../compenent/Navbar"
import Main from "../../compenent/Main"
import Section from "../../compenent/Section"

export default function Dashboard() {
    return (
        <>
            <Navbar title='Filmes' />
            <Main />
            <Section />
        </>
    )
}