import {Box, Typography, Button} from '@mui/material';
//import React,{useState} from 'react'

const generos = [
    { id: 1, nome: "Ação" },
    { id: 2, nome: "Aventura" },
    { id: 3, nome: "Animação" },
    { id: 4, nome: "Comédia" },
    { id: 5, nome: "Crime" },
    { id: 6, nome: "Documentário" },
    { id: 7, nome: "Drama" },
    { id: 8, nome: "Familía" },
    { id: 9, nome: "Fantasia" },
    { id: 10, nome: "História" },
    { id: 11, nome: "Terror" },
    { id: 12, nome: "Música" },
    { id: 13, nome: "Mistério" },
    { id: 14, nome: "Romance" },
    { id: 15, nome: "Ficcção Cientifica" },
    { id: 16, nome: "Cinema TV" },
    { id: 17, nome: "Thriller" },
    { id: 18, nome: "Guerra" },
    { id: 19, nome: "Faroeste" }
];


export default function Main() { 
    //const [actual, setAtual] = useState(0)

    return (
        <div>
            <Box fullWidth 
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'column',
                    rowGap: 4,
                    height: 450,
                    bgcolor: '#6E0AB8'
                }}>
                <Typography color='#fff' textAlign='center'
                    sx={{ fontSize: '3em', fontWeight: 'bold' }}
                >
                    Milhões de filmes, séries e pessoas <br /> para descobrir. Explore já.
                </Typography>

                <Typography color='#fff' textTransform='uppercase' textAlign='center' sx={{ fontSize: '.7em', marginBottom: -2 }}>
                    Filtre Por:
                    </Typography>
                <Box 
                    sx={{
                        paddingInline: '9%',
                         display: 'flex',
                         flexWrap: 'wrap',
                         gap: '10px',
                         justifyContent: 'center'
                    }}>
                    
                    {generos.map((genero) => {
                            /* const handleClick = () => {
                                console.log('clicou')
                                setAtual(actual + 1)
                                console.log('clicou' + setAtual);
                            } */
                            return (
                                <Button
                                 sx={{
                                    background: '#fff',
                                    color: '#090422',
                                    fontWeight: 'bold',
                                    '&:hover': {
                                        backgroundColor: 'rgba(0, 0, 0, .5)',
                                        color: '#fff'
                                    }
                                 }} /* onClick={handleClick} */ variant="contained"
                                >{genero.nome}
                                </Button>
                            )
                    })}
                </Box>
            </Box>
        </div>
    )
}