import {Box, AppBar, Toolbar, Typography} from '@mui/material';

export default function Navbar({color, title}) {
    //console.log(title)
    return (
        <div>  
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static" sx={{background: '#090422'}}>
                <Toolbar>
                    <img width='50px' src="https://i.imgur.com/0S6Q5be.png" alt="Ícone da filmes" />
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, textTransform: 'uppercase' }}>
                        {title}
                    </Typography>
                </Toolbar>
                </AppBar>
            </Box>
        </div>
    )
}