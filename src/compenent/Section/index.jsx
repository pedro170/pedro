import {Box, Card, CardContent, CardMedia, Typography} from '@mui/material';
import React, {useEffect, useState} from 'react'
import axios from 'axios'


export default function Section() {

    const [filmes, setFilmes] = useState([])
    
    useEffect(() => {
      axios.get('https://api.themoviedb.org/3/movie/popular?api_key=719fbd9994e6f0b7f1d4738980e75453')
      .then(response => {
        setFilmes(response.data.results)
      }).catch(error => {
        console.log(error);
      })
    }, [])

    return (
      <>
        <Box fullWidth
          sx={{
            position: 'relative',
            top: '20px',
            paddingInline: 10
          }}
        >
          { filmes.length > 0 && 
            filmes.map((filmes) => {
              return (
                <Box>
                  <Card
                    sx={{
                       width: 200,
                       boxShadow: 'none'
                    }}
                  >
                  <CardMedia
                    component="img"
                    height="260"
                    image="https://i.imgur.com/3JVXp2b.png"
                    alt="green iguana"

                    sx={{
                      borderBottomLeftRadius: '4px',
                      borderBottomRightRadius: '4px'
                    }}
                  />
                  <CardContent>
                    <Typography variant="body2" color="text.secondary" fontWeight='bold'>
                      <Typography color='#000' fontWeight='bold'><p>{filmes.title}</p></Typography>
                      <p>{filmes.release_date}</p>
                      <p>{filmes.genre_ids}</p>
                    </Typography>
                  </CardContent>
                </Card>
                </Box>
              )
            })
          }
        </Box>
      </>
    )
}


/* 

const filmes = [
    {id: 1, adulto: true, title: 'Homem de Ferro', data: '2010-12-15', genero: [28, 12, 878, 10, 20] },
    {id: 2, adulto: false, title: 'Capitão América', data: '2015-01-16', genero: [28, 10, 20] },
    {id: 3, adulto: true, title: 'Homem Aranha', data: '2009-07-13', genero: [12, 20] },
    {id: 4, adulto: true, title: 'Pantera Negra', data: '2005-05-12', genero: [12] },
    {id: 5, adulto: false, title: 'Víuva negra', data: '2014-11-05', genero: [28, 12, 10, 20] },
    {id: 6, adulto: true, title: 'Planeta dos Macacos', data: '2008-08-18', genero: [28, 12, 878, 10, 20] },
    {id: 7, adulto: false, title: 'Avatar', data: '2009-12-18', genero: [28, 12, 878, 20] },
    {id: 9, adulto: true, title: 'Hora do rush', data: '2007-04-06', genero: [10, 20] },
    {id: 10, adulto: true, title: 'Homem de Ferro 2', data: '2007-10-10', genero: [28, 12, 20] },
    {id: 11, adulto: false, title: 'A noiva', data: '2000-01-02', genero: [28, 12, 878, 10, 20] },
    {id: 12, adulto: true, title: 'Titanic', data: '1990-03-15', genero: [28] },
    {id: 13, adulto: false, title: 'Django livre', data: '2016-05-20', genero: [878, 10] },
    {id: 14, adulto: false, title: 'Homem Aranha 2', data: '2018-09-30', genero: [28, 12, 878, 10, 20] },
    {id: 15, adulto: true, title: 'Homem de Aranha: Sem volta ao lar', data: '2021-12-16', genero: [28, 878, 10] }
  ]

  console.log(filmes);

  const genero = filmes.map(g => g.genero);
  console.log(genero);

  const filterGenero = genero.filter(fg => {
    if(fg.genero[878, 10] == 1)
    console.log(fg.filterGenero);
  })

  const filterGenero = genero.filter()
  console.log(filterGenero, 'teste'); 

  const adulto = filmes.map(a => a.adulto);
  console.log(adulto);
*/