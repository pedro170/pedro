import {Routes, Route} from 'react-router-dom'
import Dashboard from '../pages/Dashboard'

//import  from '@material'

export default function Router()  {
    return(
        <Routes /* path='*' */ >
            <Route path="/" element={<Dashboard />} />
        </Routes>
    )
}