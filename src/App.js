import {BrowserRouter} from 'react-router-dom'
import Route from './routes/Routes';

function App() {
  return (
    <BrowserRouter>
      <Route />
    </BrowserRouter>
  );
}

export default App;